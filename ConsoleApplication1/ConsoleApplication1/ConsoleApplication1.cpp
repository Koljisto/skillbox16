﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
#include <iomanip>
const int N = 10;

void printArray(int arr[N][N]) {
    for (size_t i = 0; i < N; i++)
    {
        for (size_t j = 0; j < N; j++)
        {
			std::cout << std::setw(5) << arr[i][j] << "   ";
        }
        std::cout << std::endl;
    }
}

void printArrayLine(int arr[N][N], int line) {
        for (size_t j = 0; j < N; j++)
        {
            std::cout << std::setw(5) << arr[line][j] << "   ";
        }
}

int main()
{
    time_t now = time(NULL);
    struct tm *ltm = localtime(&now);
    int arr[N][N];
    for (size_t i = 0; i < N; i++)
    {
        for (size_t j = 0; j < N; j++)
        {
            arr[i][j] = i + j;
        }
    }
    int day = ltm->tm_mday;
    printArray(arr);
    std::cout << "Print line index by " << day << std::endl;
    printArrayLine(arr, day % N);
    //Если надо прям не по дню с единицы, а по индексу (1=0), то просто уменьшить переменную day на единицу
}